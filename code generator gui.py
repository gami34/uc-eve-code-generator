from tkinter import *
from PIL import Image, ImageTk
import os

main_menu = ['file','edit','format','run','options','windows','help']


class App:
    def __init__(self, master):
        self.monitor = 0
        self.per_monitor = 0
        self.con_value = 0
        self.master = master
        self.master.configure(bg = '#000000')
        self.master.title(' : uC Eve - '+os.getcwd())
        self.master.iconbitmap(r'C:\Users\Gideon\Desktop\python examples\code generator\icon.ico')
        self.frame = Frame(self.master,bg='#000000')
        self.frame.pack(side = TOP, fill = X)
        self.middle = Frame(self.master,bg = '#000000')
        self.middle.pack(side = TOP, fill =BOTH, expand=YES)
        self.questionare = Frame(self.master, bg='#000000')
        self.questionare.pack(side=TOP, fill = X)
        self.statuser = Frame(self.master,bg='#000000')
        self.statuser.pack(side = BOTTOM, fill =X)
        
        
        self.subvalues = [['New File    Ctrl + N',"Open...  Ctrl + O","Save    Ctrl + O","Save As     Ctrl + O","Print","Exit","Close"],["Undo    Ctrl + Z","Redo    Ctrl + Y","Cut     Ctrl + X","Copy    Ctrl + C","Paste   Ctrl + Z","Select All     Ctrl + A","Find     Ctrl + F","Find Again   Ctrl + G","Find Selection   Ctrl + F3","Replace         Ctrl + H","Go to Line       Alt  + G","Show completion  Ctrl + Space","Expand the Word  Alt + /","Show call Tip    Ctrl + backspace","show surrounding paren   Ctrl + 0"],["Indent Region          Ctrl + [","Dedent Region          Ctrl + ]","Comment Out Region     Alt + 3","Uncomment Out Region     Alt + 4","Tabify Region     Alt + 5","Untabify Region     Alt + 6" ,"Toggle Tab     Alt + T","New Indent Width     Alt + U","Format Paragraph     Alt + Q","Strip Trailing white space"],["Python Shell","Check Module  Alt + X","Run Module  F5"],["configure IDLE","Code Context"],["Zoom Height    Alt + F2"],["About Uc Eve","IDLE help","Python Docs    F1","Uc Eve Demo"]]
       

        self.menu_object()
        self.status_bar()
        self.contents()
        self.peripherals()

    def say_hi(self,x):
        self.x = x
        self.textarea.delete(1.0, END)
        self.textarea.insert(1.0,self.x+' Programming, Please work with the command prompt below, while I Generate Your code')

    def menu_object(self):
        files = os.listdir('./icons')
        self.holder = [0]*len(files)
        self.iconer = [0]*len(files)
        self.submenu = [0]*len(files)
        for filename in files:
            
            self.holder[self.monitor] = PhotoImage(file =r'./icons/'+filename)
            self.iconer[self.monitor] = Menubutton(self.frame, image = self.holder[self.monitor],bg='#000000')
            #self.iconer[self.monitor].bind('<Enter>',lambda x = self.iconer[self.monitor] : self.design(x))
            self.submenu[self.monitor] = Menu(self.iconer[self.monitor], bg='#ffffff', tearoff = 0)
            self.iconer[self.monitor].config(menu=self.submenu[self.monitor])
            self.iconer[self.monitor].pack(side=LEFT, padx = 10, pady = 4)
            if len(files)==len(self.subvalues):
                for lists in self.subvalues[self.monitor]:
                    self.submenu[self.monitor].add_command(label = lists)
            else:
                print('they are not equal')
            self.monitor = self.monitor + 1

    def status_bar(self):
        self.label = Label(self.statuser,text='X : 0, Y : 0', font=('Verdena',8 ),bg='orange')
        self.label.pack(side=RIGHT)

    def design(self, event):
        self.obj = obj
        self.obj.configure(bg='#560000')

    def changer(self,event):
        
        self.con_value +=1
        self.temp = self.quest.get(1.0,END)
        self.textarea.insert(END,event.keysym)
        
        

    def contents(self):
        self.textarea =  Text(self.middle, bd=2)
        self.textarea.pack(side=LEFT,fill=BOTH, expand = YES, pady=30)
        self.quest = Text(self.questionare, bd =2, bg ='#121212', font=('verdena',14),height=6,fg='#ffffff')
        self.quest.pack(side=TOP, fill =X)
        self.quest.insert(1.0, 'Hello New User, Welcome to uC Eve, what is your name ? >>> ')
        self.quest.bind('<Key>',self.changer)

    def peripherals(self):
        self.per_buttons = ['I/O Ports','Timers','interrupts','Serial Port','LCD & K-Board','ADC, DAC and sensors', 'Relay, Steppers','Wave Generation','PWM','SPI','I2C', 'Opto-Isolators']
        self.per_holder = [0]*len(self.per_buttons)
        self.buttons_frame = Frame(self.middle)
        self.buttons_frame.pack(side=LEFT, fill=Y, pady=30, padx=5)
        self.init_buttons = Button(self.buttons_frame,width=38,text="Initializer")
        self.init_buttons.grid(row=0, column = 0, columnspan = 2)
        self.final_buttons = Button(self.buttons_frame,width=38,text="Finalise")
        if len(self.per_buttons)%2 == 0:
            for ro in range(int(len(self.per_buttons)/2)):
                for co in range(2):
                    self.per_holder[self.per_monitor]=Button(self.buttons_frame, text=self.per_buttons[self.per_monitor], width=18,  command =lambda x=self.per_buttons[self.per_monitor] : self.say_hi(x))
                    self.per_holder[self.per_monitor].grid(row = ro+1, column=co)
                    self.per_monitor = self.per_monitor + 1
            self.final_buttons.grid(row=ro+2, column = 0, columnspan = 2)
        else:
            for ro in range(ceil(len(self.per_buttons)/2)+1):
                for co in range(2):
                    self.per_holder[self.per_monitor]=Button(self.buttons_frame, text=self.per_buttons[self.per_monitor],width=18, command = lambda x=self.per_buttons[self.per_monitor] :self.say_hi)
                    self.per_holder[self.per_monitor].grid(row = ro+1, column=co)
                    self.per_monitor = self.per_monitor + 1
            self.final_buttons.grid(row=ro+2, column = 0, columnspan = 2)
            
        #self.final_buttons = Button(self.buttons_frame,text="Finalise")
        #self.final_buttons.grid(row=, column = 0, columnspan = 2)
        
            
root = Tk()
app = App(root)
root.mainloop()
